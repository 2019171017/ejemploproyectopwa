import React from 'react';
import './Item.css';

function Item(props) {

    /*
    const onComplete = () => {
        alert('Tarea completada: ' + props.text);
    }

    const onDelete = () => {
        alert('Eliminaste la tarea: ' + props.text) ;
    }
    */

    return(
		<li className="Item">
            <span className={`Icon Icon-check ${props.completed && 'Icon-check--active'}`} onClick={props.onComplete}>
                √
            </span>
            <p className={`Item-p ${props.completed && 'Item-p--complete'}`}>
                {props.text}
            </p>
            <span className="Icon Icon-delete" onClick={props.onDelete}>
                <img src="https://img.icons8.com/color/48/000000/trash--v1.png" alt="eliminar" width="40px" height="40px"/>
            </span>
        </li>
	);
}

export {Item};