import React from "react";
import './Boton.css';

function Boton(props) {
    const onClickBoton = (msg) => {
        alert(msg);
    }

    return(
        <button className="Boton"
        onClick={() => onClickBoton('Aquí vas a agregar tareas')}
        >
            +
        </button>
    );
}

export {Boton};